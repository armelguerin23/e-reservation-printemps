import Bed1 from '../../../images/bed1.jpg'
import Bed2 from '../../../images/room.jpg'
import Bed3 from '../../../images/villa.jpg'
export const projectsData = [
    {
        id: 1,
        title: "Chambre reguliere",
        texte: "La chambre Régulière, plus spacieuse, vous permet de vous installer en toute tranquillité pour un séjour placé sous le signe du confort.",
        caracteristiques: ["1 lit", "une à 2 personnes ", "Climatiseur avec contrôle numérique ", "Internet haute vitesse et wi-fi gratuit"],
        description: " Nos chambres régulières ont tout ce qu’un voyageur peut attendre : confortables et fonctionnelles.Elles sont toutes équipées d’une cuisinette et d’un espace pour les repas. Parfait pour un petit séjour ou un voyage d’affaires.",
        img: Bed1
    },
    {
        id: 2,
        title: "Chambre familiale",
        texte:"Idéales pour un week-end en famille ou un plus long séjour, nos chambres familiales 4 personnes vous assurent un excellent rapport qualité-prix",
        caracteristiques: ["Une chambre avec deux lits séparés", "Climatisation ", "Superficie moyenne: 27m²","Balcon", "Deux télévisions écran plat", "Deux salles de douche"],
        description: " Idéale pour des parents et enfants, notre chambre familiale se compose de deux chambres communicantes à la décoration moderne et entièrement équipées. Vous y trouverez deux salles de douche, d'une chambre avec un grand lit double, d'une autre chambre avec deux lits séparés, de deux télévisions, deux coffres forts et deux mini bars. Notre chambre familiale dispose également d'un balcon pour pouvoir partager des moments de détente et conviviaux.",
        img: Bed2
    },
    {
        id: 2,
        title: "Suite",
        texte: "La Suite Junior dispose d’un lit double (Queen size), un coin salon doté d’un canapé confortable, 2 chaises et une table basse.",
        caracteristiques: ["Smart TV 40'","Wi-Fi haut débit gratuit", "Coffre-fort", "Réfrigérateur", "Climatisation","Café et thé gratuits dans la chambre","Matériel de repassage","Miroir de maquillage éclairé" ],
        description: " Idéale pour des parents et enfants, notre chambre familiale se compose de deux chambres communicantes à la décoration moderne et entièrement équipées. Vous y trouverez deux salles de douche, d'une chambre avec un grand lit double, d'une autre chambre avec deux lits séparés, de deux télévisions, deux coffres forts et deux mini bars. Notre chambre familiale dispose également d'un balcon pour pouvoir partager des moments de détente et conviviaux.",
        img: Bed3
    },
];